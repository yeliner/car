﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrmChapter11
{
  public  abstract  class Vehicle
    {

      public Vehicle() { }
      public Vehicle(string Color, double DailyRent, string LicenseNo, string Name, int YearsOfService) {
          this.Color = Color;
          this.DailyRent = DailyRent;
          this.LicenseNo = LicenseNo;
          this.Name = Name;
          this.YearsOfService = YearsOfService;
      }
       public string Color { get;set;}
       public double  DailyRent { get; set; }
       public string LicenseNo { get; set; }
       public string Name { get; set; }
       public int RentDate { get; set; }
       public string RentUser { get; set; }
       public int YearsOfService { get; set; }

       public abstract double cal();
    }
}
