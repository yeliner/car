﻿namespace FrmChapter11
{
    partial class FrmRent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tb = new System.Windows.Forms.TabControl();
            this.tbRent = new System.Windows.Forms.TabPage();
            this.btnRent = new System.Windows.Forms.Button();
            this.btnRefres = new System.Windows.Forms.Button();
            this.txtRentName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pRent = new System.Windows.Forms.Panel();
            this.lvRent = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tbBack = new System.Windows.Forms.TabPage();
            this.btncal = new System.Windows.Forms.Button();
            this.btnRef = new System.Windows.Forms.Button();
            this.txtRentDay = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pBack = new System.Windows.Forms.Panel();
            this.lvBack = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tbNew = new System.Windows.Forms.TabPage();
            this.btnNew = new System.Windows.Forms.Button();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbColor = new System.Windows.Forms.ComboBox();
            this.txtMoney = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTime = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtNo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.rbTruck = new System.Windows.Forms.RadioButton();
            this.rbCar = new System.Windows.Forms.RadioButton();
            this.btnExit = new System.Windows.Forms.Button();
            this.tb.SuspendLayout();
            this.tbRent.SuspendLayout();
            this.pRent.SuspendLayout();
            this.tbBack.SuspendLayout();
            this.pBack.SuspendLayout();
            this.tbNew.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(185, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "汽车租赁系统";
            // 
            // tb
            // 
            this.tb.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tb.Controls.Add(this.tbRent);
            this.tb.Controls.Add(this.tbBack);
            this.tb.Controls.Add(this.tbNew);
            this.tb.Location = new System.Drawing.Point(12, 33);
            this.tb.Name = "tb";
            this.tb.SelectedIndex = 0;
            this.tb.Size = new System.Drawing.Size(506, 366);
            this.tb.TabIndex = 2;
            // 
            // tbRent
            // 
            this.tbRent.Controls.Add(this.btnRent);
            this.tbRent.Controls.Add(this.btnRefres);
            this.tbRent.Controls.Add(this.txtRentName);
            this.tbRent.Controls.Add(this.label2);
            this.tbRent.Controls.Add(this.label4);
            this.tbRent.Controls.Add(this.pRent);
            this.tbRent.Location = new System.Drawing.Point(4, 22);
            this.tbRent.Name = "tbRent";
            this.tbRent.Padding = new System.Windows.Forms.Padding(3);
            this.tbRent.Size = new System.Drawing.Size(498, 340);
            this.tbRent.TabIndex = 0;
            this.tbRent.Text = "租金";
            this.tbRent.UseVisualStyleBackColor = true;
            // 
            // btnRent
            // 
            this.btnRent.Location = new System.Drawing.Point(245, 279);
            this.btnRent.Name = "btnRent";
            this.btnRent.Size = new System.Drawing.Size(75, 23);
            this.btnRent.TabIndex = 7;
            this.btnRent.Text = "租车";
            this.btnRent.UseVisualStyleBackColor = true;
            this.btnRent.Click += new System.EventHandler(this.btnRent_Click);
            // 
            // btnRefres
            // 
            this.btnRefres.Location = new System.Drawing.Point(168, 279);
            this.btnRefres.Name = "btnRefres";
            this.btnRefres.Size = new System.Drawing.Size(66, 23);
            this.btnRefres.TabIndex = 6;
            this.btnRefres.Text = "刷新";
            this.btnRefres.UseVisualStyleBackColor = true;
            this.btnRefres.Click += new System.EventHandler(this.btnRefres_Click);
            // 
            // txtRentName
            // 
            this.txtRentName.Location = new System.Drawing.Point(222, 244);
            this.txtRentName.Name = "txtRentName";
            this.txtRentName.Size = new System.Drawing.Size(109, 21);
            this.txtRentName.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(163, 249);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "租用者";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(200, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 14);
            this.label4.TabIndex = 0;
            this.label4.Text = "可租用车辆";
            // 
            // pRent
            // 
            this.pRent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pRent.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pRent.Controls.Add(this.lvRent);
            this.pRent.Location = new System.Drawing.Point(10, 33);
            this.pRent.Name = "pRent";
            this.pRent.Size = new System.Drawing.Size(478, 199);
            this.pRent.TabIndex = 0;
            // 
            // lvRent
            // 
            this.lvRent.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.lvRent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvRent.FullRowSelect = true;
            this.lvRent.Location = new System.Drawing.Point(0, 0);
            this.lvRent.Name = "lvRent";
            this.lvRent.Size = new System.Drawing.Size(478, 199);
            this.lvRent.TabIndex = 0;
            this.lvRent.UseCompatibleStateImageBehavior = false;
            this.lvRent.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "车牌号";
            this.columnHeader1.Width = 72;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "车名";
            this.columnHeader2.Width = 77;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "颜色";
            this.columnHeader3.Width = 70;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "使用时间";
            this.columnHeader4.Width = 75;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "日租金";
            this.columnHeader5.Width = 123;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "卡车载重";
            // 
            // tbBack
            // 
            this.tbBack.Controls.Add(this.btncal);
            this.tbBack.Controls.Add(this.btnRef);
            this.tbBack.Controls.Add(this.txtRentDay);
            this.tbBack.Controls.Add(this.label3);
            this.tbBack.Controls.Add(this.label5);
            this.tbBack.Controls.Add(this.pBack);
            this.tbBack.Location = new System.Drawing.Point(4, 22);
            this.tbBack.Name = "tbBack";
            this.tbBack.Padding = new System.Windows.Forms.Padding(3);
            this.tbBack.Size = new System.Drawing.Size(498, 340);
            this.tbBack.TabIndex = 1;
            this.tbBack.Text = "还车";
            this.tbBack.UseVisualStyleBackColor = true;
            // 
            // btncal
            // 
            this.btncal.Location = new System.Drawing.Point(244, 289);
            this.btncal.Name = "btncal";
            this.btncal.Size = new System.Drawing.Size(75, 23);
            this.btncal.TabIndex = 9;
            this.btncal.Text = "选择结算";
            this.btncal.UseVisualStyleBackColor = true;
            this.btncal.Click += new System.EventHandler(this.btncal_Click);
            // 
            // btnRef
            // 
            this.btnRef.Location = new System.Drawing.Point(163, 289);
            this.btnRef.Name = "btnRef";
            this.btnRef.Size = new System.Drawing.Size(75, 23);
            this.btnRef.TabIndex = 8;
            this.btnRef.Text = "刷新";
            this.btnRef.UseVisualStyleBackColor = true;
            this.btnRef.Click += new System.EventHandler(this.btnRef_Click);
            // 
            // txtRentDay
            // 
            this.txtRentDay.Location = new System.Drawing.Point(220, 250);
            this.txtRentDay.Name = "txtRentDay";
            this.txtRentDay.Size = new System.Drawing.Size(109, 21);
            this.txtRentDay.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(161, 253);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "租用天数";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(225, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 14);
            this.label5.TabIndex = 5;
            this.label5.Text = "结算";
            // 
            // pBack
            // 
            this.pBack.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pBack.Controls.Add(this.lvBack);
            this.pBack.Location = new System.Drawing.Point(6, 32);
            this.pBack.Name = "pBack";
            this.pBack.Size = new System.Drawing.Size(479, 202);
            this.pBack.TabIndex = 4;
            // 
            // lvBack
            // 
            this.lvBack.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader12});
            this.lvBack.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvBack.FullRowSelect = true;
            this.lvBack.Location = new System.Drawing.Point(0, 0);
            this.lvBack.Name = "lvBack";
            this.lvBack.Size = new System.Drawing.Size(479, 202);
            this.lvBack.TabIndex = 1;
            this.lvBack.UseCompatibleStateImageBehavior = false;
            this.lvBack.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "车牌号";
            this.columnHeader7.Width = 72;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "车名";
            this.columnHeader8.Width = 77;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "颜色";
            this.columnHeader9.Width = 70;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "使用时间";
            this.columnHeader10.Width = 75;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "日租金";
            this.columnHeader11.Width = 123;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "卡车载重";
            // 
            // tbNew
            // 
            this.tbNew.Controls.Add(this.btnNew);
            this.tbNew.Controls.Add(this.txtWeight);
            this.tbNew.Controls.Add(this.label11);
            this.tbNew.Controls.Add(this.cbColor);
            this.tbNew.Controls.Add(this.txtMoney);
            this.tbNew.Controls.Add(this.label10);
            this.tbNew.Controls.Add(this.txtTime);
            this.tbNew.Controls.Add(this.label9);
            this.tbNew.Controls.Add(this.label8);
            this.tbNew.Controls.Add(this.txtType);
            this.tbNew.Controls.Add(this.label7);
            this.tbNew.Controls.Add(this.txtNo);
            this.tbNew.Controls.Add(this.label6);
            this.tbNew.Controls.Add(this.rbTruck);
            this.tbNew.Controls.Add(this.rbCar);
            this.tbNew.Location = new System.Drawing.Point(4, 22);
            this.tbNew.Name = "tbNew";
            this.tbNew.Size = new System.Drawing.Size(498, 340);
            this.tbNew.TabIndex = 2;
            this.tbNew.Text = "新车入库";
            this.tbNew.UseVisualStyleBackColor = true;
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(211, 287);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 23);
            this.btnNew.TabIndex = 4;
            this.btnNew.Text = "入库";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // txtWeight
            // 
            this.txtWeight.Location = new System.Drawing.Point(211, 231);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(134, 21);
            this.txtWeight.TabIndex = 14;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(118, 234);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 13;
            this.label11.Text = "卡车载重";
            // 
            // cbColor
            // 
            this.cbColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbColor.FormattingEnabled = true;
            this.cbColor.Items.AddRange(new object[] {
            "黑色",
            "白色",
            "银色",
            "灰色",
            "红色",
            "蓝色"});
            this.cbColor.Location = new System.Drawing.Point(211, 127);
            this.cbColor.Name = "cbColor";
            this.cbColor.Size = new System.Drawing.Size(134, 20);
            this.cbColor.TabIndex = 12;
            // 
            // txtMoney
            // 
            this.txtMoney.Location = new System.Drawing.Point(211, 200);
            this.txtMoney.Name = "txtMoney";
            this.txtMoney.Size = new System.Drawing.Size(134, 21);
            this.txtMoney.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(118, 203);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 12);
            this.label10.TabIndex = 10;
            this.label10.Text = "每日租金";
            // 
            // txtTime
            // 
            this.txtTime.Location = new System.Drawing.Point(211, 166);
            this.txtTime.Name = "txtTime";
            this.txtTime.Size = new System.Drawing.Size(134, 21);
            this.txtTime.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(118, 169);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 8;
            this.label9.Text = "使用时间";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(118, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 6;
            this.label8.Text = "颜色";
            // 
            // txtType
            // 
            this.txtType.Location = new System.Drawing.Point(211, 91);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(134, 21);
            this.txtType.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(118, 94);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 4;
            this.label7.Text = "车型";
            // 
            // txtNo
            // 
            this.txtNo.Location = new System.Drawing.Point(211, 55);
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(134, 21);
            this.txtNo.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(118, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 2;
            this.label6.Text = "车牌号";
            // 
            // rbTruck
            // 
            this.rbTruck.AutoSize = true;
            this.rbTruck.Location = new System.Drawing.Point(229, 23);
            this.rbTruck.Name = "rbTruck";
            this.rbTruck.Size = new System.Drawing.Size(47, 16);
            this.rbTruck.TabIndex = 1;
            this.rbTruck.Text = "吊车";
            this.rbTruck.UseVisualStyleBackColor = true;
            // 
            // rbCar
            // 
            this.rbCar.AutoSize = true;
            this.rbCar.Checked = true;
            this.rbCar.Location = new System.Drawing.Point(164, 23);
            this.rbCar.Name = "rbCar";
            this.rbCar.Size = new System.Drawing.Size(47, 16);
            this.rbCar.TabIndex = 0;
            this.rbCar.TabStop = true;
            this.rbCar.Text = "轿车";
            this.rbCar.UseVisualStyleBackColor = true;
            this.rbCar.CheckedChanged += new System.EventHandler(this.rbCar_CheckedChanged);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(439, 409);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "退出";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // FrmRent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 444);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.tb);
            this.Controls.Add(this.label1);
            this.Name = "FrmRent";
            this.Text = "FrmCar";
            this.Load += new System.EventHandler(this.FrmRent_Load);
            this.tb.ResumeLayout(false);
            this.tbRent.ResumeLayout(false);
            this.tbRent.PerformLayout();
            this.pRent.ResumeLayout(false);
            this.tbBack.ResumeLayout(false);
            this.tbBack.PerformLayout();
            this.pBack.ResumeLayout(false);
            this.tbNew.ResumeLayout(false);
            this.tbNew.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tb;
        private System.Windows.Forms.TabPage tbRent;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel pRent;
        private System.Windows.Forms.Panel pBack;
        private System.Windows.Forms.TabPage tbBack;
        private System.Windows.Forms.TabPage tbNew;
        private System.Windows.Forms.Button btnRent;
        private System.Windows.Forms.Button btnRefres;
        private System.Windows.Forms.TextBox txtRentName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btncal;
        private System.Windows.Forms.Button btnRef;
        private System.Windows.Forms.TextBox txtRentDay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rbTruck;
        private System.Windows.Forms.RadioButton rbCar;
        private System.Windows.Forms.TextBox txtTime;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtNo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMoney;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbColor;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ListView lvRent;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ListView lvBack;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
    }
}