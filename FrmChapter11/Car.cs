﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrmChapter11
{
   public class Car:Vehicle
    {
          public Car() { }
          public Car(string Color, double DailyRent, string LicenseNo, string Name, int YearsOfService)
          {

              base.Color = Color;
              base.DailyRent = DailyRent;
              base.LicenseNo = LicenseNo;
              base.Name = Name;
              base.YearsOfService = YearsOfService;
      }
          public override double cal()
          {


              return base.RentDate * DailyRent;
          }
    }
}
