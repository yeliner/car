﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrmChapter11
{
    class Truck:Vehicle
    {
         public Truck() { }
         public Truck(string Color, double DailyRent, string LicenseNo, string Name, int YearsOfService,int load)
             :base(Color,DailyRent,LicenseNo,Name,YearsOfService)
          {
              this.load = load;
            
      }
         public int load { get; set; }
         public override double cal() {


             return base.RentDate * DailyRent;
         }
    }
}
