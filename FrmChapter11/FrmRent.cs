﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FrmChapter11
{
    public partial class FrmRent : Form
    {
        public FrmRent()
        {
            InitializeComponent();
        }
        List<Vehicle> cars = new List<Vehicle>();
        List<Vehicle> bcars = new List<Vehicle>();
        private void btnExit_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void rbCar_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCar.Checked)
            {
                label11.Enabled = false;
                txtWeight.Enabled = false;
            }
            else {
                label11.Enabled = true;
                txtWeight.Enabled = true;
            }
        }

        private void btnRent_Click(object sender, EventArgs e)
        {
            if(txtRentName.Text==""){
                MessageBox.Show("请输入租用者姓名!");
                txtRentName.Focus();
                return;
            }
            if (lvRent.SelectedItems.Count > 0)
            {
           int index=lvRent.SelectedIndices[0];
           Vehicle v = cars[index];
           v.RentUser = txtRentName.Text;
           bcars.Add(v);
           cars.Remove(v);
           lvRent.SelectedItems[0].Remove();
           lvBack.Items.Clear();
           showBack();
              MessageBox.Show("租车成功!已添加至还车行列!");

            }
            else {
                MessageBox.Show("请选择要租的车辆");
            }

        }

        private void showBack()
        {
            foreach (Vehicle v in bcars)
            {
                ListViewItem items = new ListViewItem(v.LicenseNo);
                if (v is Car)
                {
                    items.SubItems.AddRange(new string[] { v.Name, v.Color, v.YearsOfService + "", v.DailyRent + "", "无" });
                }
                if (v is Truck)
                {
                    items.SubItems.AddRange(new string[] { v.Name, v.Color, v.YearsOfService + "", v.DailyRent + "", ((Truck)v).load + "" });
                }
                lvBack.Items.Add(items);
            }
        }

        private void btncal_Click(object sender, EventArgs e)
        {

            if (txtRentDay.Text == "")
            {
                MessageBox.Show("请输入租用天数!");
                txtRentDay.Focus();
                return;
            }
            if (lvBack.SelectedItems.Count>0)
            {
             int index=lvBack.SelectedIndices[0];
           Vehicle v = bcars[index];
           v.RentDate =int.Parse(txtRentDay.Text);
           cars.Add(v);
           MessageBox.Show("结算金额为:"+v.cal());
           bcars.Remove(v);
           lvBack.SelectedItems[0].Remove();
           showRent();
         

            }
            else {
                MessageBox.Show("请选择要还的车辆");
            }
        }

        private void showRent()
        {
            lvRent.Items.Clear();
            foreach (Vehicle v in cars)
            {
                ListViewItem items = new ListViewItem(v.LicenseNo);
                if (v is Car)
                {
                    items.SubItems.AddRange(new string[] { v.Name, v.Color, v.YearsOfService + "", v.DailyRent + "", "无" });
                }
                if (v is Truck)
                {
                    items.SubItems.AddRange(new string[] { v.Name, v.Color, v.YearsOfService + "", v.DailyRent + "", ((Truck)v).load + "" });
                }
                lvRent.Items.Add(items);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
           if (txtNo.Text == "")
            {
                MessageBox.Show("请输入车牌号!");
                txtNo.Focus();
                return;
            }
            if (txtType.Text == "")
            {
                MessageBox.Show("请输入车型!");
                txtType.Focus();
                return;
            }

            if (cbColor.Text == "")
            {
                MessageBox.Show("请选择颜色!");
                cbColor.Focus();
                return;
            }
            if (txtTime.Text == "")
            {
                MessageBox.Show("请输入使用时间!");
                txtTime.Focus();
                return;
            }
            try
            {
                int.Parse(txtTime.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("输入使用时间格式不正确!");
                txtTime.Focus();
                return;

            }
            if (txtMoney.Text == "")
            {
                MessageBox.Show("请输入每日租金!");
                txtMoney.Focus();
                return;
            }
           
            try
            {
                double.Parse(txtMoney.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("输入每日租金格式不正确!");
                txtMoney.Focus();
                return;

            }
            if (rbTruck.Checked)
            {

                if (txtWeight.Text == "")
                {
                    MessageBox.Show("请输入卡车载重!");
                    txtWeight.Focus();
                    return;
                }
                try
                {
                   double.Parse(txtWeight.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("输入卡车载重格式不正确!");
                    txtWeight.Focus();
                    return;

                }

            }
            if (rbCar.Checked)
            {

                Car c = new Car(cbColor.Text, double.Parse(txtMoney.Text), txtNo.Text, txtType.Text, int.Parse(txtTime.Text));
                cars.Add(c);
            }
            else {
                Truck t = new Truck(cbColor.Text, double.Parse(txtMoney.Text), txtNo.Text, txtType.Text, int.Parse(txtTime.Text),int.Parse(txtWeight.Text));
                cars.Add(t);
            }
            showRent();
                MessageBox.Show("入库成功!已添加至租车行列");

        }

      

        private void FrmRent_Load(object sender, EventArgs e)
        {
            label11.Enabled = false;
            txtWeight.Enabled = false;
        }

        private void btnRefres_Click(object sender, EventArgs e)
        {
            showRent();
        }

        private void btnRef_Click(object sender, EventArgs e)
        {
            showBack();
        }

       


        

       
    }
}
